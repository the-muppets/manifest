#!/usr/bin/python3

try:
    from gitlab import Gitlab
except ImportError:
    print("Please install the python-gitlab package via pip3")
    exit(1)

orgName = "The-Muppets"
org = Gitlab().groups.get(orgName)

file = open("default.xml", "w")
file.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
file.write("<manifest>\n")
file.write("\n")
file.write("  <remote  name=\"gitlab\"\n")
file.write("           fetch=\"..\" />\n")
file.write("\n")
file.write("  <default revision=\"master\"\n")
file.write("           remote=\"gitlab\"\n")
file.write("           sync-j=\"4\" />\n")
file.write("\n")

repos = []

for repo in org.projects.list(get_all=True):
    repos.append(orgName + "/" + repo.path)

for repo in sorted(repos):
    file.write("  <project name=\"" + repo + "\" />\n")

file.write("</manifest>\n")
file.close()
